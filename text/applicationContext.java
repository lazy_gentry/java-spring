package com.text;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * 三种使用应用上下文的方法 创建 ApplicationContext 实例（应用上下文实例）
 *
 * @author LHJ
 * @date 2022/03/28 15:07
 **/
public class applicationContext {
//获取容器对象
    public static void main(String[] args) {
        //使用应用上下文创建 应用上下文容器实例
            //第一种获取ApplicationContext对象方法-->通过类加载来创建
            ApplicationContext ac=new ClassPathXmlApplicationContext("com/text/applicationContext.xml");
            //第二种常见方法-->通过文件路径来创建（相对路径/绝对路径）
                //绝对路径
            ApplicationContext ac1=new FileSystemXmlApplicationContext("D:\\Program Files\\developTools\\java\\IntelliJ IDEA 2021.3.2\\project\\MySpring\\src\\applicationContext.xml");
                //相对路径
            //第三类方法 --》XmlWebApplicationContext 这个方法是服务器在启用时就会被常见
            ApplicationContext ac2 =new FileSystemXmlApplicationContext("src\\applicationContext.xml");
//          获取对象
            UserService us= (UserService) ac.getBean("userService");
        us.sayHello();
    }

}
