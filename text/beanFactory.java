package com.text;


import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;

/**
 * bean工厂获取bean与应用上下文获取bean的区别
 *
 * 如果我们使用beanfactory去获取bean，当你只是实例化该容器， 那么容器的bean不被实例化，
 * 只有当你去使用getBean某个bean时，才会实时的创建.
 * 而是用上下文创建时则是在加载是就把bean常见好放在内存。
 *
 * factory 有点：节约内存，只用在get时bean才被创建
 *          缺点：用时加载速度都比较慢
 * Application 优点：速度快
 *              缺点：耗内存，但90%都用上下文创建
 * @author LHJ
 * @date 2022/03/28 16:30
 **/
public class beanFactory {
    public static void main(String[] args) {
        //使用应用上下文创建实例来获取bean对象
        //ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
        //使用bean工厂来获取bean
        BeanFactory factory = new XmlBeanFactory(
                (Resource) new ClassPathXmlApplicationContext("com/text/applicationContext.xml"));
        UserService userService= (UserService) factory.getBean("userService");
        userService.sayHello();
    }
}