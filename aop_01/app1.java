package com.aop_01;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 通知_前置通知
 *
 * @author LHJ
 * @date 2022/03/31 09:04
 **/
public class app1 {
    public static void main(String[] args) {
        ApplicationContext ac=new ClassPathXmlApplicationContext("com/aop_01/beans.xml");
        TextServiceInter textServiceInter=(TextServiceInter) ac.getBean("proxyFactoryBean");
        TextServiceInter2 textServiceInter2=(TextServiceInter2) ac.getBean("proxyFactoryBean");
//这里要调用 代理对象否则没用  第二个这里的对象类型是接口，而不是实现接口发实例
        textServiceInter.sayHi();
        textServiceInter2.sayBay();
    }
}
