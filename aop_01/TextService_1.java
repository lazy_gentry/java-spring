package com.aop_01;


/**
 * @author LHJ
 * @date 2022/03/31 09:08
 **/
public class TextService_1 implements TextServiceInter,TextServiceInter2{
    private  String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public void sayHi() {
        System.out.println("hi~"+this.name);
    }

    @Override
    public void sayBay() {
    System.out.println("Bay~"+this.name);
    }
}
