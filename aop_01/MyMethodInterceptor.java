package com.aop_01;


import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import java.lang.reflect.Method;

/**
 * 环绕通知
 *
 * @author LHJ
 * @date 2022/03/31 21:00
 **/
public class MyMethodInterceptor implements MethodInterceptor {
      @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        System.out.println("调用方法前");
        Object obj=methodInvocation.proceed();
        System.out.println("调用方法后");
        return null;
    }
}
