package com.aop_01;


import org.springframework.aop.AfterReturningAdvice;

import java.lang.reflect.Method;

/**
 * 后置事务
 *
 * @author LHJ
 * @date 2022/03/31 19:30
 **/
public class MyAfterReturning implements AfterReturningAdvice {
    @Override
    public void afterReturning(Object o, Method method, Object[] objects, Object o1) throws Throwable {
        System.out.println("后置被调用 资源被释放");
    }
}
