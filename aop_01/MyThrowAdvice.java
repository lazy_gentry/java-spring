package com.aop_01;


import org.springframework.aop.ThrowsAdvice;

import java.lang.reflect.Method;

/**
 * 异常通知
 *
 * @author LHJ
 * @date 2022/03/31 21:16
 **/
public class MyThrowAdvice implements ThrowsAdvice {
    public void afterThrowing(Method method, Object[] args,Object target,  Exception e){
        System.out.println("出现异常"+e.getMessage());
    }
}
