package com.aop_01;


import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * 代理对象
 *
 * @author LHJ
 * @date 2022/03/31 13:06
 **/
public class MyMethodBeforeAdvice implements MethodBeforeAdvice {
    @Override
    public void before(Method method, Object[] objects, Object o) {
        System.out.println("前置对象被调用，记入日志"+method.getName());
    }
}