package com.beanLife;


import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.util.Date;

/**
 * 第六步  定义一个自己的后置处理器
 * aop-->切面编程思想（对一批对象进行编程）： 只要我们在beans。xml文件中配置了这个文件，
 * 则beans中所配置的所有bean对象都要经过该对象，每一个对象经过都要运行一遍
 * 作用：1.批量给经过对象添加函数，属性
 *      2.类似于过滤器，可以过滤所有对象id
 *      3.用来书写日志，计入每一个对象被实例化的实间
 *
 * @author LHJ
 * @date 2022/03/28 21:11
 **/
public class MyBeansPostProcess implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {
        System.out.println("第六步：postProcessBeforeInitialization函数被调用");

        return o;
    }

    @Override
    public Object postProcessAfterInitialization(Object o, String s) throws BeansException {
        System.out.println("第九部：postProcessAfterInitialization函数被调用"+
                o+"对象创建的时间是"+new java.util.Date());
        return o;

    }
}
