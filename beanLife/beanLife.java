package com.beanLife;


import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 单例bean的生命周期（单例是生命周期最长的一类，在最糟糕的情况下单例生命周期可以达到11步）
 *
 * @author LHJ
 * @date 2022/03/28 19:27
 **/
public class beanLife implements BeanNameAware , BeanFactoryAware, ApplicationContextAware, InitializingBean, DisposableBean {
//属性和get set方法
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    //定义一个方法
    public void sayHi(){
        System.out.println("hi~"+name);
    }
//如果你实现了bean名字关注接口(BeanNameAware) 则，可以通过setBeanName获取id号
    @Override
    public void setBeanName(String s) {
        System.out.println("第三步：setBeanName:  "+s);
    }
//④	如果你实现了 bean工厂关注接口，(BeanFactoryAware),则可以获取BeanFactory
    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("第四步：setBeanFactory:  "+beanFactory);
    }
    //⑤	如果你实现了 ApplicationContextAware接口，则调用方法
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("第五步：applicationContext:  "+applicationContext);
    }
// ⑥	如果bean 和 一个后置处理器关联,则会自动去调用 Object postProcessBeforeInitialization方法
//⑦	如果你实现InitializingBean 接口，则会调用 afterPropertiesSet
    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("第七步：afterPropertiesSet函数被调用");
    }
    //⑧	如果自己在<bean init-method=”init” /> 则可以在bean定义自己的初始化方法.
    public void MyInit(){
        System.out.println("第八步：我自己的init方法被调用，这与myInit方法在class里的位置和方法顺序无关");
    }
//第十步应用bean
    @Override
    public void destroy() throws Exception {
        System.out.println("第十一 实现DisposableBean接口调用destroy函数用来关闭bean释放资源，关闭流");
    }
    public void MyDestroy(){
        System.out.println("第十二  ：调用自定义的MyDestroy方法来有针对的关闭释放资源，" +
                "类似MyInit需要在beans中配置");
    }
}
