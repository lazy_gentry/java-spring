package com.beanLife;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 调用beanLife
 *
 * @author LHJ
 * @date 2022/03/28 20:44
 **/
public class add1 {
    public static void main(String[] args) {
        ApplicationContext ac=new
                ClassPathXmlApplicationContext("com/beanLife/beans.xml");
        beanLife n= (beanLife) ac.getBean("beanLife");
        n.setName("李浩杰");   //为bean对象设置属性可以用它自身的set，get方法外 还可以在beans文件中设置属性
        n.sayHi();
    }

}
