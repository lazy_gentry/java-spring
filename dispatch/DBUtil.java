package com.dispatch;


/**
 * @author LHJ
 * @date 2022/03/29 20:17
 **/
public class DBUtil {

    private  String url;
    private String driverName;
    private String username;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


}
