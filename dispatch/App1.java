package com.dispatch;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 使用spring的特殊bean,完成分散配置
 *
 * @author LHJ
 * @date 2022/03/29 20:00
 **/
public class App1 {
    public static void main(String[] args) {
        ApplicationContext ac= new ClassPathXmlApplicationContext("com/dispatch/beans.xml");
        DBUtil dbUtil= (DBUtil) ac.getBean("dbutil");
        System.out.println(dbUtil.getDriverName()+""+ dbUtil.getUrl()+""+ dbUtil.getUsername());
    }
}
