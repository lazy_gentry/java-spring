package com.autowire;


/**
 * 猫
 *
 * @author LHJ
 * @date 2022/03/29 19:13
 **/
public class Cat {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
}
