package com.autowire;


/**
 * 主人类
 *
 * @author LHJ
 * @date 2022/03/29 17:03
 **/
public class Master {
    private String name;
    private Dog dog;
    private Cat cat;

    public Master(Cat cat, Dog dog){
        this.cat = cat;
        this.dog=dog;
    }
    public Master(Dog dog){
        this.dog = dog;
    }
    public Master(){

    }
    //get set
    public Cat getCat() {
        return cat;
    }
    public void setCat(Cat cat) {
        this.cat = cat;
    }
    public Dog getDog() {
        return dog;
    }

    public void setDog(Dog dog) {
        this.dog = dog;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
