package com.autowire;


/**
 * 狗类
 *
 * @author LHJ
 * @date 2022/03/29 17:03
 **/
public class Dog {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
}
