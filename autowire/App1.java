package com.autowire;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * main/ 五种自动装配bean的用法
 *
 * @author LHJ
 * @date 2022/03/29 16:59
 **/
public class App1 {
    public static void main(String[] args) {
        ApplicationContext ac=new ClassPathXmlApplicationContext("com/autowire/beans.xml");
        Master master = (Master) ac.getBean("master");
        System.out.println(master.getName()+"养了"+master.getDog().getName()+"和"+master.getCat().getName());
    }
}
