package com.collection;


/**
 * 雇员类
 *
 * @author LHJ
 * @date 2022/03/28 23:55
 **/
public class Employee  {
    private String name;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
