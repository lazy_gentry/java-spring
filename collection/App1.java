package com.collection;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Iterator;
import java.util.Map;

/**
 * 主函数
 *
 * @author LHJ
 * @date 2022/03/28 23:09
 **/
public class App1 {
    public static void main(String[] args) {
        ApplicationContext ac=new ClassPathXmlApplicationContext("com/collection/beans.xml");
        Department department= (Department) ac.getBean("department");
        System.out.println( department.getName());
        System.out.println("............从集合取出...............");
        for(String emName:department.getEmptyName()){
            System.out.println(emName);
        }
        System.out.println("............从List取出...............");
        for (Employee e:department.getEmployeeList()){
            System.out.println(e.getName()+e.getId());
        }
        System.out.println("............从set取出...............");
        for (Employee e:department.getEmployeeSet()){
            System.out.println(e.getName()+e.getId());
        }
        System.out.println("............从Map取出...............");
        //方法一：使用entry从map里取值
        for (Map.Entry<String,Employee> e:department.getEmployeeMap().entrySet()) {
            System.out.println(e.getKey()+""+e.getValue().getName()+""+e.getValue().getId());
        }
        //方法二：使用迭代器
        Map<String,Employee> employeeMap= department.getEmployeeMap();
        Iterator<String> iterator =employeeMap.keySet().iterator();
        while(iterator.hasNext()){
            String key = iterator.next();
            Employee employee=employeeMap.get(key);
            System.out.println(employee.getName()+""+employee.getId());

        }

    }
}
