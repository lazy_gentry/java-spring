package com.collection;


import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 雇员集合
 *1，用数组这种简单数据类型
 * @author LHJ
 * @date 2022/03/28 23:10
 **/
public class Department {
   private String name;
    private String [] EmptyName;
    private List<Employee> employeeList;
    private Set<Employee> employeeSet;

    public Map<String, Employee> getEmployeeMap() {
        return employeeMap;
    }

    public void setEmployeeMap(Map<String, Employee> employeeMap) {
        this.employeeMap = employeeMap;
    }

    private Map<String, Employee> employeeMap;

    public Set<Employee> getEmployeeSet() {
        return employeeSet;
    }

    public void setEmployeeSet(Set<Employee> employeeSet) {
        this.employeeSet = employeeSet;
    }
    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getEmptyName() {
        return EmptyName;
    }

    public void setEmptyName(String[] emptyName) {
        EmptyName = emptyName;
    }
}
