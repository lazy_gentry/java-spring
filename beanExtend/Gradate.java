package com.beanExtend;


/**
 * student子类
 *
 * @author LHJ
 * @date 2022/03/29 10:21
 **/
public class Gradate extends Student{
    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    protected String high;
}
