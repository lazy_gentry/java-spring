package com.beanExtend;


/**
 * 父类
 *
 * @author LHJ
 * @date 2022/03/29 10:19
 **/
public class Student {
    protected String name;
    protected int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
