package com.beanExtend;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * main函数 演示bean类继承关系
 *
 * @author LHJ
 * @date 2022/03/29 10:17
 **/
public class app1 {
    public static void main(String[] args) {
        ApplicationContext ac=new ClassPathXmlApplicationContext("com/beanExtend/beans.xml");
        Gradate graph= (Gradate) ac.getBean("gradate");
        System.out.println(graph.name+""+graph.age);
    }
}
