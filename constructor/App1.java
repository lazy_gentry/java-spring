package com.constructor;


import com.collection.Department;
import com.collection.Employee;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Iterator;
import java.util.Map;

/**
 * 主函数  除了在beans中使用set方法给bean注入值外，还可以通过构造函数注入
 * 构造函数的选择与赋值也是在beans.xml文件中配置
 *
 * @author LHJ
 * @date 2022/03/28 23:09
 **/
public class App1 {
    public static void main(String[] args) {
        ApplicationContext ac=new ClassPathXmlApplicationContext("com/constructor/beans.xml");
        com.constructor.Employee employee= (com.constructor.Employee) ac.getBean("employee");
        System.out.println(employee.getName());

    }
}
