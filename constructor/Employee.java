package com.constructor;


/**
 * 雇员类
 *
 * @author LHJ
 * @date 2022/03/28 23:55
 **/
public class Employee {
    private String name;
    private int id;
    //默认构造函数
    public Employee() {}
    //一个参数的构造函数
    public Employee(String name) {
        this.name = name;
    }
    //两个参数的构造函数
    public Employee(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
